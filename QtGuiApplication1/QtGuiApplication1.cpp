#include "QtGuiApplication1.h"
#include "MenuWidgets/MenuSimpleToolButton.h"
#include "MenuWidgets/MenuFilterWidget.h"
#include "MenuWidgets/MenuPageListWidget.h"
#include "Filters/FilterSettings.h"

QtGuiApplication1::QtGuiApplication1(QWidget *parent)
	: QMainWindow(parent)
{
	MenuSimpleToolButton *viewer = new MenuSimpleToolButton(NULL, "View", "PACSViewer", "viewer.png");
	MenuSimpleToolButton *refresh = new MenuSimpleToolButton(NULL, "Refresh", "Refresh", "sync.png");
	
	MenuFilterWidget *filters = new MenuFilterWidget;
	connect(filters, SIGNAL(MoreFiltersPressed()), SLOT(SlotMoreFiltersPressed()));

	MenuPageListWidget *pages = new MenuPageListWidget;
	
	QHBoxLayout *hl = new QHBoxLayout;
	hl->setMargin(0);
	hl->addWidget(viewer);
	hl->addSpacing(2);
	hl->addWidget(pages);
	hl->addSpacing(2);
	hl->addWidget(refresh);
	hl->addSpacing(2);
	hl->addWidget(filters);
	hl->addStretch();

	QWidget *sourceTab = new QWidget;
	sourceTab->setLayout(hl);
	sourceTab->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

	QTabWidget *tw = new QTabWidget;
	tw->addTab(sourceTab, "Sources");
	tw->addTab(new QWidget(), "Analysis");
	tw->addTab(new QWidget(), "Print");
	tw->addTab(new QWidget(), "Tools");
	tw->setStyleSheet("QTabWidget::tab-bar {left: 40px;}");

	QWidget *tableWidget = new QWidget;
	tableWidget->setStyleSheet("background: grey;");
	tableWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	QWidget *cw = new QWidget;
	QVBoxLayout *vl = new QVBoxLayout;
	vl->setSpacing(0);
	vl->setMargin(0);
	vl->addWidget(tw);
	vl->addWidget(tableWidget, 1);
	cw->setLayout(vl);

	setCentralWidget(cw);

	resize(QSize(1200,800));
}


void QtGuiApplication1::SlotMoreFiltersPressed()
{
	auto *sett = new FilterSettingsDialog;
	sett->show();
}