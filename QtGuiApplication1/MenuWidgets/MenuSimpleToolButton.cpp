#include "MenuSimpleToolButton.h"
#include "styleSheets.h"
#include <QVBoxLayout>

MenuSimpleToolButton::MenuSimpleToolButton(QWidget *parent, const QString &label, const QString &button, const QString &icon)
	: QWidget(parent)
{
	m_button = StyledToolButton(button, icon, 32);
		
	new QToolButton;
	m_button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

	m_label = StyledLabel(label);
	
	QVBoxLayout *vl = new QVBoxLayout;
	vl->setSpacing(0);
	vl->setMargin(0);

	vl->addStretch(2);
	vl->addWidget(m_button);
	vl->addStretch(2);
	vl->addWidget(m_label);

	setLayout(vl);
	//setStyleSheet("border: 1px solid grey");
}

MenuSimpleToolButton::~MenuSimpleToolButton()
{
}