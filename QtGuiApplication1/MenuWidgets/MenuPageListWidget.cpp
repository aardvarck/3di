#include <QVBoxLayout>
#include "MenuPageListWidget.h"
//#include "styleSheets.h"
#include "ui/styleSheets.h"


MenuPageListWidget::MenuPageListWidget(QWidget *parent)
	: QWidget(parent)
{
	m_label = StyledLabel("Page navigation");

	m_ltButton = new QToolButton;
    m_ltButton->setIcon(QIcon(":/QtGuiApplication1/resources/page_backward_backward.png"));
    connect(m_ltButton, SIGNAL(clicked()),
            this,       SLOT(clickBtn()));

	m_rtButton = new QToolButton;
    m_rtButton->setIcon(QIcon(":/QtGuiApplication1/resources/page_forward_forward.png"));
    connect(m_rtButton, SIGNAL(clicked()),
            this,       SLOT(clickBtn()));

	m_lbButton = new QToolButton;
    m_lbButton->setIcon(QIcon(":/QtGuiApplication1/resources/page_backward_stop.png"));
    connect(m_lbButton, SIGNAL(clicked()),
            this,       SLOT(clickBtn()));

	m_rbButton = new QToolButton;
    m_rbButton->setIcon(QIcon(":/QtGuiApplication1/resources/page_forward_stop.png"));
    connect(m_rbButton, SIGNAL(clicked()),
            this,       SLOT(clickBtn()));

	m_spinBox = new QSpinBox;
	m_spinBox->setFixedWidth(60);

	QVBoxLayout *leftVl = new QVBoxLayout;
	leftVl->addWidget(m_ltButton);
	leftVl->addWidget(m_lbButton);

	QVBoxLayout *rightVl = new QVBoxLayout;
	rightVl->addWidget(m_rtButton);
	rightVl->addWidget(m_rbButton);
	
	QHBoxLayout *hl = new QHBoxLayout;
	hl->setSpacing(1);
	hl->setMargin(0);
	hl->addLayout(leftVl);
	hl->addWidget(m_spinBox);
	hl->addLayout(rightVl);

	QVBoxLayout *mVl = new QVBoxLayout;
	mVl->setMargin(0);
	mVl->addStretch();
	mVl->addLayout(hl);
	mVl->addStretch();
	mVl->addWidget(m_label);

	setLayout(mVl);
}

MenuPageListWidget::~MenuPageListWidget()
{
}

void MenuPageListWidget::clickBtn(){
    QToolButton * btn =  qobject_cast<QToolButton*>(sender());
    if(btn==m_ltButton)
        emit previousPage();
    if(btn==m_rtButton)
        emit nextPage();
    if(btn==m_lbButton)
        emit lastPage();
    if(btn==m_rbButton)
        emit firstPage();
}

QToolButton *MenuPageListWidget::leftBtn()
{
    return m_ltButton;
}

QToolButton *MenuPageListWidget::rightBtn()
{
    return m_rtButton;
}
