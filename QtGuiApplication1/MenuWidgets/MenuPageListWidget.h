#pragma once

#include <QWidget>
#include "ui_PageListWidget.h"

class MenuPageListWidget : public QWidget
{
	Q_OBJECT

public:
	MenuPageListWidget(QWidget *parent = Q_NULLPTR);
	~MenuPageListWidget();

private:
	QToolButton *m_ltButton, *m_rtButton, *m_lbButton, *m_rbButton;
	QSpinBox *m_spinBox;
	QLabel *m_label;
};
