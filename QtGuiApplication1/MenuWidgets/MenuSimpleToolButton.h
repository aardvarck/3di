#pragma once

#include <QWidget>
#include <QToolButton>
#include <QLabel>

class MenuSimpleToolButton : public QWidget
{
	Q_OBJECT

public:
	MenuSimpleToolButton(QWidget *parent, const QString &label, const QString &button, const QString &icon);
	~MenuSimpleToolButton();

private:
	QLabel *m_label;
	QToolButton *m_button;
};
