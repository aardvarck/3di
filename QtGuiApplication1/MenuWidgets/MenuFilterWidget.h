#pragma once

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QToolButton>

class MenuFilterWidget : public QWidget
{
	Q_OBJECT

public:
	MenuFilterWidget(QWidget *parent = Q_NULLPTR);
	~MenuFilterWidget();

signals:
	void MoreFiltersPressed();

private:
	QLineEdit *m_FioEdit, *m_patIdEdit, *m_birthEdit, *m_dateEdit, *m_stDescEdit, *m_serDescEdit, *m_misEdit, *m_appEdit;
	QToolButton *m_selBtn, *m_newBtn, *m_applyBtn, *m_removeBtn, *m_clearBtn, *m_saveBtn, *m_moreBtn, *m_modalityBtn;
	QLabel *m_label;
};
