#include "MenuFilterWidget.h"
#include <QLineEdit>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QToolButton>
#include "styleSheets.h"

MenuFilterWidget::MenuFilterWidget(QWidget *parent)
	: QWidget(parent)
{
	m_label = StyledLabel("Filters");

	m_selBtn = StyledToolButton("My filter", "filters/filter_empty.png", 32);
	m_selBtn->setPopupMode(QToolButton::MenuButtonPopup);
	
	auto makeEdit = []()-> QLineEdit* { 
		QLineEdit *le = new QLineEdit;
		le->setMaximumWidth(100);
		return le;
	};
	m_FioEdit = makeEdit();
	m_patIdEdit = makeEdit();
	m_birthEdit = makeEdit();
	m_dateEdit = makeEdit();
	m_stDescEdit = makeEdit();
	m_modalityBtn = new QToolButton;
	m_serDescEdit = makeEdit();
	m_misEdit = makeEdit();
	m_appEdit = makeEdit();

	auto makeLabel = [](const QString &txt)-> QLabel* {
		QLabel *l = new QLabel(txt);
		l->setAlignment(Qt::AlignCenter);
		return l;
	};
	
	QGridLayout *gl = new QGridLayout;
	gl->setMargin(0);
	gl->setHorizontalSpacing(2);
	gl->setVerticalSpacing(0);
	gl->addWidget(makeLabel("FIO"), 0, 0);
	gl->addWidget(m_FioEdit, 0, 1);
	gl->addWidget(makeLabel("Patient ID"), 1, 0);
	gl->addWidget(m_patIdEdit, 1, 1);
	gl->addWidget(makeLabel("Birth"), 2, 0);
	gl->addWidget(m_birthEdit, 2, 1);
	
	gl->addWidget(makeLabel("Study date"), 0, 2);
	gl->addWidget(m_dateEdit, 0, 3);
	gl->addWidget(makeLabel("Study desc"), 1, 2);
	gl->addWidget(m_stDescEdit, 1, 3);
	gl->addWidget(makeLabel("Modality"), 2, 2);
	gl->addWidget(m_modalityBtn, 2, 3);

	gl->addWidget(makeLabel("Serie desc"), 0, 4);
	gl->addWidget(m_serDescEdit, 0, 5);
	gl->addWidget(makeLabel("MIS #"), 1, 4);
	gl->addWidget(m_misEdit, 1, 5);
	gl->addWidget(makeLabel("Appoint"), 2, 4);
	gl->addWidget(m_appEdit, 2, 5);

	auto makeToolButton = [](const QString &txt, const QString &icon)-> QToolButton* {
		QToolButton *tb = StyledToolButton(txt, icon, 32, Qt::ToolButtonTextBesideIcon);
		tb->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
		return tb;
	}; 
	m_newBtn = makeToolButton("New", "filters/filter_add.png");
	m_applyBtn = makeToolButton("Apply", "filters/filter_empty.png");
	m_removeBtn = makeToolButton("Remove", "filters/filter_remove.png");
	m_clearBtn = makeToolButton("Clear", "filters/filter_empty.png");
	m_saveBtn = makeToolButton("Save", "filters/filter_empty.png");
	m_moreBtn = makeToolButton("More..", "filters/filter_sett.png");
	connect(m_moreBtn, SIGNAL(pressed()), SIGNAL(MoreFiltersPressed()));

	QGridLayout *gl2 = new QGridLayout;
	gl2->setSpacing(0);
	gl2->addWidget(m_newBtn, 0, 0, 1, 1);
	gl2->addWidget(m_applyBtn, 1, 0, 1, 1);
	gl2->addWidget(m_removeBtn, 0, 2, 1, 1);
	gl2->addWidget(m_clearBtn, 1, 2, 1, 1);
	gl2->addWidget(m_saveBtn, 0, 3, 1, 1);
	gl2->addWidget(m_moreBtn, 1, 3, 1, 1);

	QHBoxLayout *hl = new QHBoxLayout;
	hl->setMargin(0);
	hl->setSpacing(10);
	hl->addWidget(m_selBtn);
	hl->addLayout(gl);
	hl->addLayout(gl2);

	QVBoxLayout *mainVl = new QVBoxLayout;
	mainVl->setMargin(0);
	mainVl->setSpacing(0);
	mainVl->addStretch();
	mainVl->addLayout(hl);
	mainVl->addStretch();
	mainVl->addWidget(m_label);

	setLayout(mainVl);
}

MenuFilterWidget::~MenuFilterWidget()
{
}
