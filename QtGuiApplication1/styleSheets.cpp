#include "styleSheets.h"

QLabel* StyledLabel(const QString &txt)
{
	QLabel *label = new QLabel(txt);
	label->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 white, stop: 1 grey);"
							"border-style: solid;"
							"border-color: grey;"
							"border-width: 1px;"
							"border-bottom-left-radius: 5px;"
							"border-bottom-right-radius: 5px;");
	label->setAlignment(Qt::AlignCenter);
	return label;
}

QToolButton *StyledToolButton(const QString &text, const QString &iconPath, int size, Qt::ToolButtonStyle style )
{
	QToolButton *tb = new QToolButton;
	tb->setText(text);
	tb->setToolButtonStyle(style);

	QPixmap bgPixmap(":/QtGuiApplication1/resources/" + iconPath);
	QPixmap scaled = bgPixmap.scaled(QSize(size, size), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	QIcon icon(scaled);
	tb->setIcon(icon);
	tb->setIconSize(QSize(size, size));

	return tb;
}
