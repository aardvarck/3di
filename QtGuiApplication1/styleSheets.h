#pragma once

#include <QLabel>
#include <QToolButton>

QLabel* StyledLabel(const QString &txt);

QToolButton *StyledToolButton(const QString &text, const QString &iconPath, int size, Qt::ToolButtonStyle style = Qt::ToolButtonTextUnderIcon);