#include "FilterSettings.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QComboBox>


FilterSettingsDialog::FilterSettingsDialog(QWidget *parent)
	: QDialog(parent)
{
	
	
	m_Save = new QPushButton("Save");
	m_Apply = new QPushButton("Apply");
	m_Clear = new QPushButton("Clear");
	m_Close = new QPushButton("Close");

	m_Tw = new QTreeWidget;
	m_Tw->setColumnCount(2);

	Filter f;
	SetFilterInfo(f);

	
	auto *hl = new QHBoxLayout;
	hl->addWidget(m_Save);
	hl->addWidget(m_Apply);
	hl->addWidget(m_Clear);
	hl->addWidget(m_Close);
	
	auto *vl = new QVBoxLayout;
	vl->addWidget(m_Tw);
	vl->addLayout(hl);

	setWindowTitle("Additional filter parameters");

	setLayout(vl);
	setFixedSize(400, 600);

}

FilterSettingsDialog::~FilterSettingsDialog()
{


}

void AddTreeChild(QTreeWidget *tr, QTreeWidgetItem *parent, FilterEntryType type, const QString &name, const QString &value)
{
	QTreeWidgetItem *t = new QTreeWidgetItem(parent);
	t->setText(0, name);

	switch (type)
	{
	case String:
		break;
	case Date:
		break;
	case Enum: {
		QComboBox *cmb = new QComboBox;
		tr->setItemWidget(t, 1, cmb);
		break;
	}
	case Int:
		break;
	default:
		break;
	}
}

void FilterSettingsDialog::SetFilterInfo(const Filter &f)
{
	m_Tw->clear();
	
	QTreeWidgetItem *t1 = new QTreeWidgetItem();
	t1->setText(0, "Filter");
	m_Tw->insertTopLevelItem(0, t1);
	AddTreeChild(m_Tw, t1, FilterEntryType::String, "Filter name", f.filterName);
	AddTreeChild(m_Tw, t1, FilterEntryType::Enum, "Default filter", QString(f.deafultFilter));
	
	QTreeWidgetItem *t2 = new QTreeWidgetItem(m_Tw);
	t2->setText(0, "Patient");
	AddTreeChild(m_Tw, t2, FilterEntryType::Date, "Birthdate from", f.birthFrom.toString());
	AddTreeChild(m_Tw, t2, FilterEntryType::Date, "Birthdate to", f.birthTo.toString());
	AddTreeChild(m_Tw, t2, FilterEntryType::Enum, "Sex", QString(f.sex));

	QTreeWidgetItem *t3 = new QTreeWidgetItem(m_Tw);
	t3->setText(0, "Study");
	AddTreeChild(m_Tw, t3, FilterEntryType::Date, "Related group", f.birthFrom.toString());
	AddTreeChild(m_Tw, t3, FilterEntryType::Date, "Related user", f.birthTo.toString());
	AddTreeChild(m_Tw, t3, FilterEntryType::Date, "Study date cm", f.birthFrom.toString());
	AddTreeChild(m_Tw, t3, FilterEntryType::Date, "Study date to", f.birthTo.toString());
	AddTreeChild(m_Tw, t3, FilterEntryType::Enum, "Status", f.status);
	AddTreeChild(m_Tw, t3, FilterEntryType::String, "Send doctor", f.sendDoctor);
	AddTreeChild(m_Tw, t3, FilterEntryType::String, "Diagn doctor", f.diagnDoctor);
	AddTreeChild(m_Tw, t3, FilterEntryType::String, "Manufacturer", f.manufact);
	AddTreeChild(m_Tw, t3, FilterEntryType::Int, "Last load period", QString(f.lastLoadDays));
	AddTreeChild(m_Tw, t3, FilterEntryType::String, "Priority", f.priority);
	AddTreeChild(m_Tw, t3, FilterEntryType::String, "Institution", f.institute);

	QTreeWidgetItem *t4 = new QTreeWidgetItem(m_Tw);
	t4->setText(0, "Series");
	AddTreeChild(m_Tw, t4, FilterEntryType::Date, "Modality", f.modality);
	AddTreeChild(m_Tw, t4, FilterEntryType::Date, "Body part", f.bodyPart);
	AddTreeChild(m_Tw, t4, FilterEntryType::Date, "Series date from", f.seriesFrom.toString());
	AddTreeChild(m_Tw, t4, FilterEntryType::Date, "Series date to", f.seriesTo.toString());
	AddTreeChild(m_Tw, t4, FilterEntryType::Enum, "Called AE", f.calledAE);
	AddTreeChild(m_Tw, t4, FilterEntryType::String, "Caller AE", f.callerAE);
	AddTreeChild(m_Tw, t4, FilterEntryType::String, "Radiologist", f.radiologist);
	AddTreeChild(m_Tw, t4, FilterEntryType::String, "Number", f.number);
	AddTreeChild(m_Tw, t4, FilterEntryType::Int, "Image date from", f.imageFrom.toString());
	AddTreeChild(m_Tw, t4, FilterEntryType::String, "Image date to", f.imageTo.toString());
	AddTreeChild(m_Tw, t4, FilterEntryType::Enum, "Use on previous study", QString(f.useOnPrevStudy));
}