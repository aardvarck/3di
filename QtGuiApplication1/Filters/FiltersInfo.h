#pragma once

#include <QDialog>
#include <QPushButton>
#include <QTreeWidget>
#include <QDate>
#include <QMap>


struct Filter
{
	QString filterName;
	bool deafultFilter;

	QDate birthFrom, birthTo;
	QString sex;

	QString relGroup, relUser;
	QDate studyFrom, studyTo;
	QString status;
	QString sendDoctor, diagnDoctor;
	QString manufact, priority, institute;
	int lastLoadDays;

	QString modality, bodyPart;
	QDate seriesFrom, seriesTo;
	QString callerAE, calledAE;
	QString radiologist, number;
	QDate imageFrom, imageTo;
	bool useOnPrevStudy;

	/*QString getString(const QString &key) { return m_options[key]; }
	QDate getDate(const QString &key) { return QDate::fromString(m_options[key]); }
	int getInt(const QString &key) { return m_options[key].toInt(); }*/

private:
	//QMap<QString, QString> m_options;
};

enum FilterCategories {
	Main,
	Patient,
	Study,
	Series
};

enum FilterEntryType {
	String,
	Date,
	Enum,
	Int
};

struct FilterEntry {
	FilterEntry(FilterCategories c, FilterEntryType t, const QString &n, const QString &v = "") { category = c; name = n; value = v; type = t; }

	FilterCategories category;
	QString name, value;
	FilterEntryType type;
};

//public FilterSettings MakeFilterSettings();

class FilterSettings
{
public:
	FilterSettings();
	
	void addEntry(FilterEntry &entry);

private:
	QMap <FilterCategories, QList<FilterEntry> > m_entries;
};
