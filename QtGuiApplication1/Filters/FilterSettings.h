#pragma once

#include <QDialog>
#include <QPushButton>
#include <QTreeWidget>
#include "FiltersInfo.h"

class FilterSettingsDialog : public QDialog
{
	Q_OBJECT

public:
	FilterSettingsDialog(QWidget *parent = Q_NULLPTR);
	~FilterSettingsDialog();

	void SetFilterInfo(const Filter &f);

private:
	QPushButton *m_Save, *m_Apply, *m_Clear, *m_Close;
	QTreeWidget *m_Tw;

};
